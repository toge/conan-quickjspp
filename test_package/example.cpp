#include <iostream>


#include "quickjspp.hpp"

int main() {
    auto runtime = qjs::Runtime {};
    auto context = qjs::Context { runtime };

    return 0;
}