import os

from conans import ConanFile, tools
import shutil

class QuickJSPPConan(ConanFile):
    name           = "quickjspp"
    version        = "20200218"
    license        = "MIT"
    author         = "toge.mail@gmail.com"
    url            = "https://bitbucket.org/toge/conan-quickjspp"
    description    = "QuickJS C++ wrapper "
    topics         = ("quickjs", "wrapper")
    no_copy_source = True
    requires       = "quickjs/2020-01-19@toge/stable"

    # No settings/options are necessary, this is header only

    def source(self):
        # tools.get("https://github.com/ftk/quickjspp/archive/v{}.tar.gz".format(self.version))
        # shutil.move("quickjspp-{}".format(self.version), "quickjspp")

        tools.download("https://raw.githubusercontent.com/ftk/quickjspp/b43c1ddd554fc25bb732a7b3c08d1a9d18bf5888/quickjspp.hpp", "quickjspp.hpp")

    def package(self):
        self.copy("*.hpp", src=".", dst="include", keep_path=False)

    def package_info(self):
        self.info.header_only()

